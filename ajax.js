// fonction qui permet de comparer deux valeurs et en déterminer le plus grand ou le plus petit
function compareValues(key, order='asc') {
  return function(a, b) {
    if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
        return 0; 
    }

    const varA = (typeof a[key] === 'string') ? 
      a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'st ring') ? 
      b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order == 'desc') ? (comparison * -1) : comparison
    );
  };
}


function tab_trie (event){
  var arrayOfMedailles = new Array();
  var features ;
  var classement = 0 ;
  
  var ajax = new XMLHttpRequest() ;
  ajax.open('GET', 'serveur.php', true) ;
  ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  ajax.addEventListener('readystatechange',  function(e) {
      
    if(ajax.readyState == 4 && ajax.status == 200) {
          //console.log(ajax.responseText) ;
          features = JSON.parse(ajax.responseText) ;
          //console.log(features) ;
          var tableau = document.getElementById("retour") ;
          tableau.innerHTML = '';
          var tab_recap = document.createElement("table")
            // permet de parcourir le tableau d'élements              
           for (var i = 0; i<features.length; i++ ){
               classement += 1 ;
               //tri du tableau selon le cas
               if (features[i].BRONZE == features[i].BRONZE){
                features.sort(compareValues('NAME', 'asc'));             
               if (features[i].SILVER == features[i].SILVER){
                features.sort(compareValues('BRONZE', 'desc'));
                if (features[i].GOLD == features[i].GOLD){
                  features.sort(compareValues('SILVER', 'desc'));
                if (features[i].GOLD >= 0){
                  features.sort(compareValues('GOLD', 'desc'));            
               
               // création des lignes du tableau
               var row = tab_recap.insertRow(-1);
               var classementCell = row.insertCell(-1);
               classementCell.appendChild(document.createTextNode(classement)) ;
               var nameCell = row.insertCell(-1);
               nameCell.appendChild(document.createTextNode(features[i].NAME)) ;
               var goldCell = row.insertCell(-1);
               goldCell.appendChild(document.createTextNode(features[i].GOLD)) ;
               var silverCell = row.insertCell(-1);
               silverCell.appendChild(document.createTextNode(features[i].SILVER)) ;
               var bronzeCell = row.insertCell(-1);
               bronzeCell.appendChild(document.createTextNode(features[i].BRONZE)) ;
               arrayOfMedailles.push([classement,features[i].NAME,features[i].GOLD,features[i].SILVER,features[i].BRONZE]);
                // ajout d'un élement tableau à la page html
            }
              }
          }
          }
          }
            console.log(tab_recap) ;
            tableau.appendChild(tab_recap); 
            }
});
  ajax.send();
}

tab_trie() ;

var tri_or = document.getElementById("or") ;
//var tri_bronze = document.getElementById("bronze") ;
// exécution de la fonction trie or 
tri_or.addEventListener("click", trie_or) ;

// function qui permet d'afficher la liste des pays ayant reçu au moins une médaille d'or
function trie_or(event){
  var features ;
  var classement = 0 ;
 
  var ajax = new XMLHttpRequest() ;
  ajax.open('GET', 'serveur.php', true) ;
  ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  ajax.addEventListener('readystatechange',  function(e) {
      
    if(ajax.readyState == 4 && ajax.status == 200) {
          console.log(ajax.responseText) ;
          features = JSON.parse(ajax.responseText) ;
          console.log(features) ;
          var tableau = document.getElementById("retour") ;
          tableau.innerHTML = '';
          var tab_recap = document.createElement("table") ;
            // permet de parcourir le tableau d'élements  
            for (var i = 0; i<features.length; i++ ){
               // classement des pays selon le nombre de médailles en or obtenues
               features.sort(compareValues('GOLD', 'desc'));
               classement += 1 ;
            // ajout d'un élement tableau à la page html
               
            // affiche les pays qui ont obtenus au moins une médaille d'or
                if (features[i].GOLD >= 1){
                  // création des lignes du tableau
                  var row = tab_recap.insertRow(-1);
                  var classementCell = row.insertCell(-1);
                  classementCell.appendChild(document.createTextNode(classement)) ;
                  // insertion des lignes dans le tableau
                  var nameCell = row.insertCell(-1);
                  nameCell.appendChild(document.createTextNode(features[i].NAME)) ;  
                  var goldCell = row.insertCell(-1);
                  goldCell.appendChild(document.createTextNode(features[i].GOLD)) ;                
                  var silverCell = row.insertCell(-1);
                  silverCell.appendChild(document.createTextNode(" ")) ;                 
                  var bronzeCell = row.insertCell(-1);
                  bronzeCell.appendChild(document.createTextNode("")) ;                 
                }                               
                }
                //insertion du tableau dans la page html
                tableau.appendChild(tab_recap);
    }        
});
  ajax.send();
}

// ajout d'un evenement sur le bouton argent de la page html
var tri_argent = document.getElementById("argent") ; 

tri_argent.addEventListener("click", trie_silver) ;
/*fonction qui trie la liste des pays selon le nombre de medailles 
d'argent et affiche ceux qui en possède au moins une*/
function trie_silver(event){
  var features ;
  var classement = 0 ;
 
  var ajax = new XMLHttpRequest() ;
  ajax.open('GET', 'serveur.php', true) ;
  ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  ajax.addEventListener('readystatechange',  function(e) {
      
    if(ajax.readyState == 4 && ajax.status == 200) {
          console.log(ajax.responseText) ;
          features = JSON.parse(ajax.responseText) ;
          console.log(features) ;
          var tableau = document.getElementById("retour") ;
          tableau.innerHTML = '';
          var tab_recap = document.createElement("table") ;
            // permet de parcourir le tableau d'élements  
            for (var i = 0; i<features.length; i++ ){
               // classement des pays selon le nombre de médailles d'argent obtenues
               features.sort(compareValues('SILVER', 'desc'));
               classement += 1 ;                              
                // affiche les pays qui ont obtenus au moins une médaille d'argent
                if (features[i].SILVER >= 1){
                  var row = tab_recap.insertRow(-1);
                  var classementCell = row.insertCell(-1);
                  classementCell.appendChild(document.createTextNode(classement)) ;
                  var nameCell = row.insertCell(-1);
                  nameCell.appendChild(document.createTextNode(features[i].NAME)) ;
                  var silverCell = row.insertCell(-1);
                  silverCell.appendChild(document.createTextNode(features[i].SILVER)) ;
                  
                  var goldCell = row.insertCell(-1);
                  goldCell.appendChild(document.createTextNode("")) ;                  
                  var bronzeCell = row.insertCell(-1);
                  bronzeCell.appendChild(document.createTextNode(" ")) ;
                }
                
                }
                //insertion du tableau dans la page html
                tableau.appendChild(tab_recap);
    }        
});
  ajax.send();
}

// ajout d'un evenement sur le bouton bronze de la page html
var triBronze = document.getElementById("bronze") ; 

triBronze.addEventListener("click", trie_bronze) ;
/*fonction qui trie la liste des pays selon le nombre de medailles 
de bronze et affiche ceux qui en possède au moins une*/
function trie_bronze(event){
  var features ;
  var classement = 0 ;
 
  var ajax = new XMLHttpRequest() ;
  ajax.open('GET', 'serveur.php', true) ;
  ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  ajax.addEventListener('readystatechange',  function(e) {
      
    if(ajax.readyState == 4 && ajax.status == 200) {
          console.log(ajax.responseText) ;
          features = JSON.parse(ajax.responseText) ;
          console.log(features) ;
          var tableau = document.getElementById("retour") ;
          tableau.innerHTML = '';
          var tab_recap = document.createElement('table') ;
            // permet de parcourir le tableau d'élements  
            for (var i = 0; i<features.length; i++ ){
               // classement des pays selon le nombre de médailles de bronze obtenues
               features.sort(compareValues('BRONZE', 'desc'));
               classement += 1 ;                 
                // affiche les pays qui ont obtenu au moins une médaille de bronze
                if (features[i].BRONZE >= 1){
                  var row = tab_recap.insertRow(-1);
                  var classementCell = row.insertCell(-1);
                  classementCell.appendChild(document.createTextNode(classement)) ;
                  var nameCell = row.insertCell(-1);
                  nameCell.appendChild(document.createTextNode(features[i].NAME)) ;
                  var bronzeCell = row.insertCell(-1);
                  bronzeCell.appendChild(document.createTextNode(features[i].BRONZE)) ;
                  }
                
                }
                //insertion du tableau dans la page html
                tableau.appendChild(tab_recap);
    }        
});
  ajax.send();
}

// ajout d'un ecouteur d'évenement sur le bouton ALL afin de reafficher le tableau d'origine
var afficherTout= document.getElementById("tout") ; 

afficherTout.addEventListener("click", afficher_tout) ;

function afficher_tout(event){
  tab_trie() ;
  }        
